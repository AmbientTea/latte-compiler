all: latc

latc:
	ghc -prof -auto-all -caf-all --make src/Main.hs -o latc_llvm -isrc

clean:
	-rm -f src/*.o src/*.hi *.o
	-rm -f *.log *.aux *.hi *.dvi *.ps *.info
	-rm -f test/*.class test/*.j
