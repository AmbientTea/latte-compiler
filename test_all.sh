#!/bin/bash

make || exit;

echo ==========good============
for file in `ls good/*.lat`; do
	echo -n $file ...
	./latc_llvm $file > /dev/null
done;

echo ==========bad=============
for file in `ls bad/*.lat`; do
	echo -n $file ... 
	./latc_llvm $file > /dev/null
done;
