module Common where

import Data.Maybe
import Data.Map as Map

import AbsLatte

data StrInfo = StrInfo {
	content :: String,
	strLabel :: String,
	len :: Int
} deriving Show

data TransEnv = TransEnv {
	functions :: [(String, FuncType)],
    nextReg :: Integer,
    nextBr :: Integer,
    varFrames :: FrameStack String VarInfo,
    nextVarNum :: Map.Map String Integer,
    expectedRet :: Type,
    nextStr :: Int,
    strConsts :: Map.Map String StrInfo,
    returned :: Bool
} deriving (Show)

initialTransEnv = TransEnv {
	functions = [],
    nextReg = 0,
    nextBr = 1,
    varFrames = [[]],
    nextVarNum = Map.empty,
    expectedRet = Int,
    nextStr = 0,
    strConsts = Map.empty,
    returned = False
}

resetEnv env = env {
    nextReg = 0,
    nextBr = 1,
    varFrames = [[]],
    nextVarNum = Map.empty,
    expectedRet = Int,
    returned = False
}

data VarInfo = VarInfo {
    varId :: String,
    varType :: Type,
    isAlloc :: Bool
} deriving (Show)


data FuncType = FuncType {
	retType :: Type,
	argTypes :: [Type],
	extern :: Bool
} deriving (Show)


type FrameStack a b = [[(a, b)]]

pushFrame s = [] : s
popFrame = tail

lookupInFramesMaybe id s = Prelude.lookup id $ concat s
lookupInFrames id s = case lookupInFramesMaybe id s of
	Just j -> j
	Nothing -> error $ show id ++ " " ++ show s
addToFrame id v s = case s of
	_:_ -> ((id, v) : head s) : tail s
	[] -> error $ show v ++ "  " ++ show s
