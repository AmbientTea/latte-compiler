module TypeChecker where

import Control.Monad
import Data.List

import AbsLatte
import PrintLatte

import Common hiding (returned, functions)


data Env = Env {
	functions :: [(String, FuncType)],
	variables :: [[(String, Type)]],
	currentFun :: String,
	returnType :: Type,
	returned :: Bool
	} deriving (Show)

emptyEnv = Env {
	functions = [
		("printInt", FuncType {retType = Void, argTypes = [Int], extern = True}),
		("printString", FuncType {retType = Void, argTypes = [Str], extern = True}),
		("readInt", FuncType {retType = Int, argTypes = [], extern = True}),
		("readString", FuncType {retType = Str, argTypes = [], extern = True})
		],
	variables = [],
	currentFun = "***none***",
	returnType = Void,
	returned = False
	}

pushVariableFrame env = env { variables = [] : variables env }
popVariableFrame env = env { variables = tail $ variables env }
addVariable id typ env = case variables env of
	[] -> throwErr env ["frontend bug: no variable frame"]
	h:t -> case lookup id h of
		Just _ -> throwErr env ["variable", id, "already declared"]
		Nothing -> return $ env {variables = ((id,typ):h):t}

lookupVariable id env = case lookup id $ concat $ variables env of
	Nothing -> throwErr env ["variable", id, "not found"]
	Just varTp -> return varTp
lookupFunction id env = case lookup id $ functions env of
	Nothing -> throwErr env [id, "is not a function"]
	Just fTp -> return fTp

argType (Arg tp _) = tp

throwErr env msg = Left $ unwords (
	["failure in function", currentFun env, ":"] ++ msg-- ++ ["\n", show env]
	)

-------------
-- PROGRAM --
-------------

checkTypes :: Program -> Either String Env
checkTypes prog@(Program topDefs) = let
		funTypes = map unpackFun topDefs where
			unpackFun (FnDef typ (Ident id) args _) =
				(id, FuncType typ argTypes False) where
					argTypes = map argType args
		env = emptyEnv { functions = funTypes ++ functions emptyEnv }
	in do
		v <- mapM (checkTypesFun env) topDefs
		return env
---------------
-- FUNCTIONS --
---------------
argToPair (Arg tp (Ident id)) = (id, tp)

checkTypesFun :: Env -> TopDef -> Either String Env
checkTypesFun env (FnDef typ (Ident id) args (Block stmts)) = let
		nEnv = env { variables = [map argToPair args],
					 currentFun = id,
					 returnType = typ,
					 returned = (typ == Void) }
	in if args /= nub args
		then throwErr env ["function", id, "has duplicate arguments"]
		else do
			env <- checkTypeStmtList nEnv stmts
			if returned env
				then return env
				else throwErr env ["controll flow reaches end of function without return"]
----------------
-- STATEMENTS --
----------------

checkTypeStmtList :: Env -> [Stmt] -> Either String Env
checkTypeStmtList env stmts = foldM checkTypeStmt env stmts

checkTypeRetStmt :: Env -> Stmt -> Either String Env
checkTypeRetStmt env stmt = if returned env
	then throwErr env ["unreachable instruction:", printTree stmt]
	else checkTypeStmt env stmt

checkTypeStmt :: Env -> Stmt -> Either String Env
checkTypeStmt env (Empty) = return env
checkTypeStmt env (BStmt (Block stmts)) = checkTypeStmtList (pushVariableFrame env) stmts
checkTypeStmt env (Decl typ []) = return env
checkTypeStmt env (Decl typ (NoInit (Ident id):r)) = do
	nEnv <- addVariable id typ env
	checkTypeStmt nEnv (Decl typ r)
checkTypeStmt env (Decl typ (Init (Ident id) exp:r)) = do
	expTp <- checkTypeExp env exp
	nEnv <- addVariable id typ env
	if expTp /= typ
		then throwErr env ["variable", id, "of type", show typ, "initialized with expression of wrong type", show expTp]
		else checkTypeStmt nEnv (Decl typ r)

checkTypeStmt env (Ass (Ident id) exp) = do
	varTp <- lookupVariable id env
	expTp <- checkTypeExp env exp
	if varTp /= expTp
		then throwErr env ["expression", show exp, "of type", printTree expTp,
							 "assigned to variable", id, "of type", printTree varTp]
		else return env
checkTypeStmt env (Incr (Ident id)) = do
	varTp <- lookupVariable id env
	if varTp /= Int
		then throwErr env ["cannot increment variable", id, "of type", printTree varTp]
		else return env
checkTypeStmt env (Decr (Ident id)) = do
	varTp <- lookupVariable id env
	if varTp /= Int
		then throwErr env ["cannot decrement variable", id, "of type", printTree varTp]
		else return env
checkTypeStmt env (Ret exp) = do
	expTp <- checkTypeExp env exp
	if expTp == returnType env
		then return env { returned = True }
		else throwErr env ["return of type", show expTp, "when", show $ returnType env, "expected"]
checkTypeStmt env (VRet) = if returnType env == Void
	then return env { returned = True }
	else throwErr env ["void return then", show $ returnType env, "return expected"]
checkTypeStmt env (Cond exp stmt) = checkTypeStmt env (CondElse exp stmt Empty)
checkTypeStmt env (CondElse exp lstm rstm) = do
	expTp <- checkTypeExp env exp
	lstmEnv <- checkTypeStmt (pushVariableFrame env) lstm
	rstmEnv <- checkTypeStmt (pushVariableFrame env) rstm
	let nEnv = env { returned = case exp of
					ELitTrue -> returned lstmEnv
					ELitFalse-> returned rstmEnv
					_ -> returned lstmEnv && returned rstmEnv }
	if expTp /= Bool
		then throwErr env ["non-boolean expression", printTree exp, "of type", printTree expTp, "as if-else condition"]
		else return nEnv
checkTypeStmt env (While exp stmt) = do
	expTp <- checkTypeExp env exp
	stmtEnv <- checkTypeStmt (pushVariableFrame env) stmt
	if expTp /= Bool
		then throwErr env ["non-boolean expression", printTree exp, "of type", printTree expTp, "as while condition"]
		else return env
checkTypeStmt env (SExp exp) = do
	expTp <- checkTypeExp env exp
	return env

-----------------
-- EXPRESSIONS --
-----------------


checkTypeExp :: Env -> Expr -> Either String Type
checkTypeExp env (EVar (Ident id)) = lookupVariable id env

checkTypeExp env (ELitInt _  ) = return Int
checkTypeExp env (ELitTrue   ) = return Bool
checkTypeExp env (ELitFalse  ) = return Bool
checkTypeExp env (EString str) = return Str

checkTypeExp env exp@(EApp (Ident id) exprs) = do
	fTp <- lookupFunction id env
	expTps <- mapM (checkTypeExp env) exprs
	if argTypes fTp == expTps
		then return $ retType fTp
		else throwErr env ["arguments", printTree expTps, "cannot be applied to", id]

checkTypeExp env (Neg exp) = checkTypeUnOp env exp "!" Int Int
checkTypeExp env (Not exp) = checkTypeUnOp env exp "!" Bool Bool

checkTypeExp env (EMul lexp Times rexp) = checkTypeBinOp env lexp rexp "*" [Int] Int
checkTypeExp env (EMul lexp Div rexp)   = checkTypeBinOp env lexp rexp "/" [Int] Int
checkTypeExp env (EMul lexp Mod rexp)   = checkTypeBinOp env lexp rexp "%" [Int] Int

checkTypeExp env (EAdd lexp Plus rexp)  = do
	ltp <- checkTypeExp env lexp
	rtp <- checkTypeExp env rexp
	if ltp == rtp && ltp `elem` [Int, Str]
		then return ltp
		else throwErr env ["operator + can not be applied to arguments",
			printTree lexp, "and", printTree rexp, "of types", show ltp, "and", show rtp]

checkTypeExp env (EAdd lexp Minus rexp) = checkTypeBinOp env lexp rexp "-" [Int] Int

checkTypeExp env (ERel lexp LTH rexp) = checkTypeBinOp env lexp rexp "<" [Int] Bool
checkTypeExp env (ERel lexp LE rexp)  = checkTypeBinOp env lexp rexp "<=" [Int] Bool
checkTypeExp env (ERel lexp GTH rexp) = checkTypeBinOp env lexp rexp ">" [Int] Bool
checkTypeExp env (ERel lexp GE rexp)  = checkTypeBinOp env lexp rexp ">=" [Int] Bool

checkTypeExp env (ERel lexp EQU rexp) = checkTypeBinOp env lexp rexp "==" [Int,Bool] Bool
checkTypeExp env (ERel lexp NE rexp)  = checkTypeBinOp env lexp rexp "!=" [Int,Bool] Bool

checkTypeExp env (EAnd lexp rexp) = checkTypeBinOp env lexp rexp "*" [Bool, Int] Bool
checkTypeExp env (EOr lexp rexp)  = checkTypeBinOp env lexp rexp "*" [Bool, Int] Bool

checkTypeUnOp env exp op tp rtp = do
	t <- checkTypeExp env exp
	if t == tp
		then return rtp
		else throwErr env ["operator", op, "can not be aplied to argument", printTree exp, "of type", show tp]

checkTypeBinOp env lexp rexp op tps rettp = do
	ltp <- checkTypeExp env lexp
	rtp <- checkTypeExp env lexp
	if ltp == rtp && ltp `elem` tps
		then return rettp
		else throwErr env ["operator", op, "can not be applied to arguments",
			printTree lexp, "and", printTree rexp, "of types", show ltp, "and", show rtp]
