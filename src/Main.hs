{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import System.IO
import System.Environment

import Data.List

import ErrM
import LexLatte
import ParLatte
import AbsLatte

import TypeChecker
import Translate
import Generate

import Common



main :: IO ()
main = do
	args <- getArgs
	input <- case args of
		[] -> getContents
		f:_ -> readFile f
	
	let t = do
		tree <- parseIt input
		env <- checkTypes tree
		let funs = TypeChecker.functions env
		let nEnv = initialTransEnv { Common.functions = funs }
		return $ transProg nEnv tree
	
	
	case t of
		Left err -> do
			hPutStrLn stderr "BAD"
			hPutStrLn stderr err
		Right (env, t) -> do
			hPutStr stderr $ show env
			hPutStrLn stderr "OK"
			putStrLn $ generateAll t env
	return ()



parseIt :: String -> Either String Program
parseIt input = case pProgram $ myLexer input of
		ErrM.Bad err -> Left "SYNTAX ERROR"
		ErrM.Ok tree -> Right tree
