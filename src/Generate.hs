module Generate where


import Control.Monad
import Control.Monad.Identity
import Control.Monad.Trans.State
import Data.List
import Data.Map as Map hiding (filter, map)

import AbsLatte
import PrintLatte

import Common

data IVar =
	  Reg Integer
	| Lit Integer
	| Var String
	| Glob String
	| VoidReg

data ICode =
	  IOp IVar Type String IVar IVar
	| IRet Type IVar
	| IVRet
	| ILabel String
	| IJumpIf IVar String String
	| IJump String
	| IAlloc IVar Type
	| IStore Type IVar IVar
	| ILoad IVar Type IVar
	| IFun Type String [(Type, String)]
	| IFunEnd
	| ICall IVar Type String [(Type, IVar)]
	| ICallVoid String [(Type, IVar)]
	| IStrCast IVar Int IVar

genTp Int = "i32"
genTp Bool = "i1"
genTp Str = "i8*"
genTp Void = "void"

genV v = case v of
	Reg i -> "%" ++ show i
	Lit i -> show i
	Var s -> "%" ++ s
	Glob s -> "@" ++ s
	VoidReg -> error "void"

-- @s1 = private constant [7 x i8] c"Hello,\00"
genStringDecl inf = unwords ["@" ++ strLabel inf, "=", "private constant [", show $ len inf, " x i8]",
	"c\"" ++ (content inf) ++ "\""]

genFunDecl (id, inf) =
	unwords ["declare", genTp $ retType inf, "@" ++ id, "(", intercalate "," $ map genTp $ argTypes inf, ")"]

generateAll :: [ICode] -> TransEnv -> String
generateAll tree env = let
		strings = map (genStringDecl . snd) $ Map.toList $ strConsts env
		externs = map genFunDecl $ filter (extern . snd) $ functions env
	in intercalate "\n" $ strings ++ externs ++ (map generate tree)


generate c = case c of
	IOp t tp op v1 v2 -> unwords [genV t, "=", op, genTp tp, genV v1, ",", genV v2]
	IRet tp v -> unwords ["ret", genTp tp, genV v]
	IVRet -> "ret"
	ILabel l -> "\n" ++ l ++ ":"
	IJumpIf v l1 l2 -> unwords ["br i1", genV v, ", label",  "%" ++ l1, ",label", "%" ++ l2]	
	IJump l -> "br label %" ++ l
	IAlloc v t -> unwords [genV v, "=", "alloca", genTp t]
	IStore tp v1 v2 -> unwords ["store", genTp tp, genV v1, ",", genTp tp ++ "*", genV v2]
	ILoad v1 tp v2 -> unwords [genV v1, "=", "load", genTp tp ++ "*", genV v2]
	IFun tp id tps -> unwords ["define", genTp tp, "@" ++ id, "(",
		intercalate "," (map (\(tp,id) -> genTp tp ++ " %" ++ id) tps),
		")", "{"]
	IFunEnd -> "}"
	ICall v tp id args -> unwords [genV v, "=", "call", genTp tp, "@" ++ id, "(", 
		intercalate "," (map (\(tp,vr) -> genTp tp ++ " " ++ genV vr) args),
		")"]
	ICallVoid id args -> unwords ["call void @" ++ id, "(", 
		intercalate "," (map (\(tp,vr) -> genTp tp ++ " " ++ genV vr) args),
		")"]
	IStrCast v l s -> unwords [genV v, "=", "bitcast", "[" ++ (show l) ++ " x i8]*", genV s, "to", "i8*"]
