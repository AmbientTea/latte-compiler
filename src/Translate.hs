module Translate where

import AbsLatte

import Control.Monad.Trans.Class
import Control.Monad.Identity
import Control.Monad.Trans.State
import Control.Monad.Trans.Writer
import Control.Monad.Trans.Reader

import qualified Data.Map as Map
import Data.Maybe

import Common
import Generate




type Eval a = StateT TransEnv (WriterT [ICode] Identity) a


freshTemp :: Eval IVar
freshTemp = do
	env <- get
	put $ env { nextReg = nextReg env + 1 }
	return $ Reg $ nextReg env

freshLabel = do
	env <- get
	put $ env { nextBr = nextBr env + 1 }
	return $ show $ nextBr env

freshVarNum id = do
	env <- get
	let varNums = nextVarNum env
	let (num, nNums) = case Map.lookup id varNums of
		Nothing -> (1, Map.insert id 2 varNums)
		Just i -> (i, Map.insert id (i+1) varNums)
	put $ env { nextVarNum = nNums }
	return (id ++ show num)

pushFrame :: Eval ()
pushFrame = do
	env <- get
	put $ env { varFrames = [] : varFrames env }

popFrame :: Eval ()
popFrame = do
	env <- get
	put $ env { varFrames = tail $ varFrames env }

getVarInfo id = do
	env <- get
	return $ lookupInFrames id $ varFrames env

declareVar id tp = do
	rid <- freshVarNum id
	env <- get
	put $ env { varFrames = addToFrame id (VarInfo rid tp True) $ varFrames env }
	return rid

getexpectedRet = do
	env <- get
	return $ expectedRet env

getFunInfo id = do
	env <- get
	return $ fromJust $ Prelude.lookup id $ functions env

emit = lift . tell


getStrCons :: String -> Eval StrInfo
getStrCons s = do
	env <- get
	let consts = strConsts env
	case Map.lookup s consts of
		Just i -> return i
		Nothing -> do
			let n = nextStr env
			let info = StrInfo s ("string" ++ show n) (length s)
			put $ env { nextStr = n + 1 , strConsts = Map.insert s info consts }
			return info

putLabel str = emit $ [ILabel str]
putJump str = emit $ [IJump str]

-----------------
-- EXPRESSIONS --
-----------------

transExp :: Expr -> Eval IVar
transExp (EVar (Ident id)) = do
	inf <- getVarInfo id
	if isAlloc inf
		then do
			t <- freshTemp
			emit $ [ILoad t (varType inf) (Var $ varId inf)]
			return $ t
		else return (Var $ varId inf)
transExp (ELitInt val) = return $ Lit val
transExp ELitTrue = error "true"
transExp ELitFalse = error "falsse"

transExp (EApp (Ident id) exps) = do
	expRegs <- mapM transExp exps
	finf <- getFunInfo id
	let args = zip (argTypes finf) expRegs
	case retType finf of
		Void -> do
			emit $ [ICallVoid id args]
			return VoidReg
		tp -> do
			v <- freshTemp
			emit $ [ICall v tp id args]
			return v
	

transExp (EString str) = do
	v <- freshTemp
	(StrInfo _ label len) <- getStrCons str
	emit $ [IStrCast v len (Glob label)]
	return v

transExp (Neg exp) = error "neg"
transExp (Not exp) = error "not"

transExp (EAdd exp1 Plus exp2) = transBinOp "add" Int exp1 exp2
transExp (EAdd exp1 Minus exp2) = transBinOp "sub" Int exp1 exp2

transExp (EMul exp1 Times exp2) = transBinOp "mul" Int exp1 exp2
transExp (EMul exp1 Div exp2) = transBinOp "div" Int exp1 exp2
transExp (EMul exp1 Mod exp2) = transBinOp "mod" Int exp1 exp2
	
transExp (ERel exp1 LTH exp2) = transBinOp "icmp slt" Int exp1 exp2
transExp (ERel exp1 LE exp2) = transBinOp "icmp sle" Int exp1 exp2
transExp (ERel exp1 GTH exp2) = transBinOp "icmp sgt" Int exp1 exp2
transExp (ERel exp1 GE exp2) = transBinOp "icmp sge" Int exp1 exp2

transExp (ERel exp1 EQU exp2) = transBinOp "icmp eq" Int exp1 exp2
transExp (ERel exp1 NE exp2) = transBinOp "icmp ne" Int exp1 exp2

transExp (EAnd exp1 exp2) = transBinOp "and" Bool exp1 exp2
transExp (EOr exp1 exp2) = transBinOp "or" Bool exp1 exp2


transBinOp :: String -> Type -> Expr -> Expr -> Eval IVar
transBinOp op tp e1 e2 = do
	a1 <- transExp e1
	a2 <- transExp e2
	t <- freshTemp
	emit $ [IOp t tp op a1 a2]
	return t

----------------
-- STATEMENTS --
----------------

transStmts = mapM transStmt -- XXX


transStmt :: Stmt -> Eval ()
transStmt Empty = return ()
transStmt (BStmt (Block stmts)) = do
	Translate.pushFrame
	transStmts stmts -- XXX
	Translate.popFrame
	return ()
transStmt (Decl typ decls) = do
	mapM (transDecl typ) decls
	return ()
transStmt (Ass (Ident id) exp) = do
	v <- transExp exp
	inf <- getVarInfo id
	emit $ [IStore (varType inf) v (Var $ varId inf)]
transStmt (Incr (Ident id)) = transStmt (Ass (Ident id) (EAdd (EVar (Ident id)) Plus (ELitInt 1)))
transStmt (Decr (Ident id)) = transStmt (Ass (Ident id) (EAdd (EVar (Ident id)) Minus (ELitInt 1)))
transStmt (Ret exp) = do
	v <- transExp exp
	retTp <- getexpectedRet
	freshTemp
	emit $ [IRet retTp v]
transStmt VRet = do
	freshTemp	
	emit [IVRet]
transStmt (Cond exp stmt) =  do
	v <- transExp exp
	i <- freshLabel
	let
		ifL = "if" ++ i
		endifL = "endif" ++ i
	emit $ [IJumpIf v ifL endifL]
	emit $ [ILabel $ ifL]
	transStmt stmt
	emit $ [IJump $ endifL]
	emit $ [ILabel $ endifL]
	

transStmt (CondElse exp stmt1 stmt2) = do
	v <- transExp exp
	i <- freshLabel
	let
		ifL = "if" ++ i
		elseL = "else" ++ i
		endifL = "endif" ++ i
	emit $ [IJumpIf v ifL elseL]
	emit $ [ILabel $ ifL]
	transStmt stmt1
	emit $ [IJump $ endifL]
	emit $ [ILabel elseL]
	transStmt stmt2
	emit $ [ IJump endifL ]
	emit $ [ILabel $ endifL]

transStmt (While exp stmt) = do
	i <- freshLabel
	putJump $ "while-cond" ++ i
	
	putLabel $ "while-cond" ++ i
	v <- transExp exp
	emit $ [IJumpIf v ("while-begin" ++ i) ("while-end" ++ i)]
	
	putLabel $ "while-begin" ++ i
	transStmt stmt
	putJump $ "while-cond" ++ i
	
	putLabel $ "while-end" ++ i

transStmt (SExp exp) = do
	v <- transExp exp
	return ()


-- DECLARATIONS

transDecl tp (NoInit (Ident id)) = do
	tid <- declareVar id tp
	emit $ [IAlloc (Var tid) tp]
transDecl tp (Init (Ident id) exp) = do
	v <- transExp exp
	tid <- declareVar id tp
	emit $ [IAlloc (Var tid) tp]
	emit $ [IStore tp v (Var tid)]

---------------
-- FUNCTIONS --
---------------

transFunDef (FnDef typ (Ident id) args (Block stmts)) = do
	env <- get
	put $ resetEnv env
	let argTypes = map (\(Arg tp (Ident id)) -> (tp, id)) args
	emit $ [IFun typ id argTypes]
	emit $ [ILabel $ id ++ "-begin"]
	let f (tp, id) = do
			tid <- declareVar id tp
			emit $ [IAlloc (Var tid) tp]
			emit $ [IStore tp (Var id) (Var tid)]
		in mapM f argTypes
	transStmts stmts
	emit $ [IFunEnd]

runFunDef :: TransEnv -> TopDef -> [ICode]
-- runFunDef env def = snd $ runIt (transFunDef def) env
runFunDef env def = snd $ runIt (transFunDef def) env

-------------
-- PROGRAM --
-------------

transProg :: TransEnv -> Program -> (TransEnv, [ICode])
-- transProg env (Program defs) = defs >>= (runFunDef env)
transProg env (Program defs) = let
		((_, nenv), code) = runIt (mapM transFunDef defs) env
	in (nenv, code)


----------

runIt a env = runIdentity (runWriterT (runStateT a env))
-- 3 + (2*5 - 4)
testExp = EAdd (ELitInt 3) Plus (EAdd (EMul (ELitInt 2) Times (ELitInt 5)) Minus (ELitInt 4))

-------------


